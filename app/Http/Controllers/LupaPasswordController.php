<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pengguna;

use Illuminate\Support\Str;
use App\Models\PasswordResetTokens;
use App\Mail\LupaPasswordMail;
use Illuminate\Support\Facades\Mail;

class LupaPasswordController extends Controller
{
    public function formLupapassword()
    {
        return view('pengguna.form_lupa_password');
    }

    public function prosesLupaPassword(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|string|email'
        ]);

        try {
            $datas = $req->all();
            // dd($datas);
            $cekData = Pengguna::where('email', $datas['email'])->first();
            if (empty($cekData)) {
                return redirect()->route('forgot.form-forgot')->with('error', __('Email Salah'));
            }

            $token = Str::random(64);

            $dataSave = new PasswordResetTokens;
            $dataSave->email = $datas['email'];
            $dataSave->token = $token;
            $dataSave->created_at = date('Y-m-d H:i:s');
            $dataSave->save();

            // proses kirim email
            $dataMail = [
                'nama_pengguna' => $cekData->email,
                'url_reset' => ""
            ];

            Mail::to($datas['email'])->send(new LupaPasswordMail($dataMail));
            return redirect()->route('forgot.form-forgot')->with('succes', __('silahkan periksa email anda'));
        } catch (\Throwable $th) {
            return redirect()->route('forgot.form-forgot')->with('error', __($th->getMessage()));
        }
    }
}
